﻿using System;
using System.Collections.Generic;

namespace DataAccess.Interfaces.Model
{
    public class TaskList
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public Guid UserId { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
        public virtual User User { get; set; }
    }
}
