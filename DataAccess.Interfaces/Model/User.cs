﻿using System;
using System.Collections.Generic;

namespace DataAccess.Interfaces.Model
{
    public class User
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string IconPath { get; set; }
        public virtual ICollection<TaskList> TaskLists { get; set; }
    }
}
