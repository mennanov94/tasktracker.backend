﻿using System;

namespace DataAccess.Interfaces.Model
{
    public class Task
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public bool IsCompleted { get; set; }
        public Importance Importance { get; set; }
        public DateTime DueDate { get; set; }
        public Guid TaskListId { get; set; }
        public virtual TaskList TaskList { get; set; }
    }
}
