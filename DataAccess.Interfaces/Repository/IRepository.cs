﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using JetBrains.Annotations;

namespace DataAccess.Interfaces.Repository
{
    public interface IRepository<T> where T : class
    {
        T Add([NotNull]T item);
        T Get(Guid id);
        IEnumerable<T> Get([NotNull]Expression<Func<T, bool>> predicate);
        IEnumerable<T> Get();
        T Update([NotNull]T item);
        T Delete([NotNull]T item);
        T Delete(Guid item);
    }
}
