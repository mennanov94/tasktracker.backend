﻿using System;
using System.Collections.Generic;

namespace BusinessLogic.Interfaces.Model
{
    public class TaskListDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public Guid UserId { get; set; }
        public IEnumerable<TaskDto> Tasks { get; set; }
    }
}
