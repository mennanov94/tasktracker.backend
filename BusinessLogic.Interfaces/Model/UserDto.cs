﻿using System;
using System.Collections.Generic;

namespace BusinessLogic.Interfaces.Model
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string IconPath { get; set; }
        public IEnumerable<TaskListDto> TaskList { get; set; }
    }
}
