﻿namespace BusinessLogic.Interfaces.Model
{
    public enum ImportanceDto
    {
        High = 1,
        Medium = 2,
        Low = 3
    }
}