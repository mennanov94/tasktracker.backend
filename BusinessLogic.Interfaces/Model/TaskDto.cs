﻿using System;

namespace BusinessLogic.Interfaces.Model
{
    public class TaskDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public bool IsCompleted { get; set; }
        public ImportanceDto Importance { get; set; }
        public DateTime DueDate { get; set; }
    }
}
