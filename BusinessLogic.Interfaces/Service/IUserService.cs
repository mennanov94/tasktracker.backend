﻿using System;
using BusinessLogic.Interfaces.Model;
using JetBrains.Annotations;

namespace BusinessLogic.Interfaces.Service
{
    public interface IUserService
    {
        UserDto AddUser([NotNull]UserDto user);
        UserDto GetUser(Guid id);
        UserDto UpdateUser([NotNull]UserDto user);

        bool IsUserIdExists(Guid id);
        UserDto GetUserByName(string firstName, string lastName, string middleName = null);
    }
}
