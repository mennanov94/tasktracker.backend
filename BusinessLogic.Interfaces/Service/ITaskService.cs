﻿using System;
using System.Collections.Generic;
using BusinessLogic.Interfaces.Model;
using JetBrains.Annotations;

namespace BusinessLogic.Interfaces.Service
{
    public interface ITaskService
    {
        TaskDto AddTask([NotNull]TaskDto task);
        TaskDto UpdateTask([NotNull]TaskDto task);
        TaskDto GetTask(Guid id);
        IEnumerable<TaskDto> GetTask();
        TaskDto DeleteTask([NotNull]TaskDto task);
        TaskDto DeleteTask(Guid id);
        IEnumerable<TaskDto> GetTaskByListId(Guid listId);
    }
}
