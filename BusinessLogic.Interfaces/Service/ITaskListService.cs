﻿using System;
using System.Collections.Generic;
using BusinessLogic.Interfaces.Model;
using JetBrains.Annotations;

namespace BusinessLogic.Interfaces.Service
{
    public interface ITaskListService
    {
        TaskListDto AddTaskList([NotNull]TaskListDto taskList);
        IEnumerable<TaskListDto> GetByUserId(Guid userId);
        TaskListDto DeleteTaskList(Guid id);
        TaskListDto UpdateTaskList([NotNull]TaskListDto taskListDto);
        bool IsTaskListExists(Guid listId);
    }
}
