﻿using System.Data.Entity;
using DataAccess.EntityFramework.Config;

namespace DataAccess.EntityFramework.Context
{
    public class TaskTrackerContext : DbContext
    {
        public TaskTrackerContext(string connectionString) : base(connectionString) { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserConfig());
            modelBuilder.Configurations.Add(new TaskConfig());
            modelBuilder.Configurations.Add(new TaskListConfig());

            base.OnModelCreating(modelBuilder);
        }
    }
}
