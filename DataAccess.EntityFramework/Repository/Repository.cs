﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccess.EntityFramework.Context;
using DataAccess.Interfaces.Repository;
using JetBrains.Annotations;

namespace DataAccess.EntityFramework.Repository
{
    [UsedImplicitly]
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly TaskTrackerContext _taskTrackerContext;
        private readonly DbSet<T> _dbSet;

        public Repository([NotNull]TaskTrackerContext taskTrackerContext)
        {
            _taskTrackerContext = taskTrackerContext;
            _dbSet = _taskTrackerContext.Set<T>();
        }

        public T Add(T item)
        {
            _dbSet.Add(item);
            _taskTrackerContext.SaveChanges();
            return item;
        }

        public T Get(Guid id)
        {
            return _dbSet.Find(id);
        }

        public IEnumerable<T> Get(Expression<Func<T, bool>> predicate)
        {
            return _dbSet.Where(predicate).ToList();
        }

        public IEnumerable<T> Get()
        {
            return _dbSet.ToList();
        }

        public T Update(T item)
        {
            _taskTrackerContext.Entry(item).State = EntityState.Modified;
            _taskTrackerContext.SaveChanges();
            return item;
        }

        public T Delete(T item)
        {
            var result = _dbSet.Remove(item);
            _taskTrackerContext.SaveChanges();
            return result;
        }

        public T Delete(Guid item)
        {
            var itemToRemove = Get(item);
            var result = _dbSet.Remove(itemToRemove);
            _taskTrackerContext.SaveChanges();
            return result;
        }
    }
}
