﻿using System.Data.Entity.ModelConfiguration;
using DataAccess.Interfaces.Model;

namespace DataAccess.EntityFramework.Config
{
    public class TaskListConfig : EntityTypeConfiguration<TaskList>
    {
        public TaskListConfig()
        {
            ToTable("TaskLists");

            HasKey(entity => entity.Id);

            Property(entity => entity.Title)
                .HasMaxLength(150)
                .IsRequired();

            HasRequired(entity => entity.User)
                .WithMany(user => user.TaskLists);

            HasMany(entity => entity.Tasks)
                .WithRequired(task => task.TaskList);
        }
    }
}
