﻿using System.Data.Entity.ModelConfiguration;
using DataAccess.Interfaces.Model;

namespace DataAccess.EntityFramework.Config
{
    public class TaskConfig : EntityTypeConfiguration<Task>
    {
        public TaskConfig()
        {
            ToTable("Tasks");

            HasKey(entity => entity.Id);

            Property(entity => entity.Title)
                .HasMaxLength(150)
                .IsRequired();

            HasRequired(task => task.TaskList)
                .WithMany(taskList => taskList.Tasks);
        }
    }
}
