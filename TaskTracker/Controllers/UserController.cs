﻿using System;
using System.Web.Http;
using AutoMapper;
using BusinessLogic.Interfaces.Service;
using JetBrains.Annotations;
using TaskTracker.Models;

namespace TaskTracker.Controllers
{
    public class UserController : ApiController
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(
            [NotNull]IUserService userService,
            [NotNull]IMapper mapper)
        {
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }


        [HttpGet]
        public UserViewModel Get(string firstName, string lastName)
        {
            var userDto = _userService.GetUserByName(firstName, lastName);
            var userViewModel = _mapper.Map<UserViewModel>(userDto);
            return userViewModel;
        }
    }
}