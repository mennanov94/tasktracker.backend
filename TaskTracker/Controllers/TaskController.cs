﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using AutoMapper;
using BusinessLogic.Interfaces.Model;
using BusinessLogic.Interfaces.Service;
using JetBrains.Annotations;
using TaskTracker.Models;

namespace TaskTracker.Controllers
{
    public class TaskController : ApiController
    {
        private readonly ITaskListService _taskListService;
        private readonly ITaskService _taskService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public TaskController(
            [NotNull]ITaskListService taskListService,
            [NotNull]ITaskService taskService,
            [NotNull]IUserService userService,
            [NotNull]IMapper mapper)
        {
            _taskListService = taskListService ?? throw new ArgumentNullException(nameof(taskListService));
            _taskService = taskService ?? throw new ArgumentNullException(nameof(taskService));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet]
        public IEnumerable<TaskViewModel> GetTasksByListId(Guid userId, Guid listId)
        {
            ValidateRequestAttributes(userId, listId);

            var tasksDto = _taskService.GetTaskByListId(listId);
            var tasksViewModel = _mapper.Map<IEnumerable<TaskViewModel>>(tasksDto);
            return tasksViewModel;
        }

        [HttpPut]
        public TaskViewModel CreateTask(Guid userId, Guid listId, [FromBody][NotNull]TaskViewModel taskViewModel)
        {
            ValidateRequestAttributes(userId, listId);

            var taskDto = _mapper.Map<TaskDto>(taskViewModel);
            var taskDtoInserted = _taskService.AddTask(taskDto);
            var taskViewModelInserted = _mapper.Map<TaskViewModel>(taskDtoInserted);
            return taskViewModelInserted;
        }

        [HttpGet]
        public TaskViewModel DeleteTask(Guid userId, Guid listId, Guid taskId)
        {
            ValidateRequestAttributes(userId, listId);

            var taskDto = _taskService.DeleteTask(taskId);
            var taskViewModel = _mapper.Map<TaskViewModel>(taskDto);
            return taskViewModel;
        }

        [HttpPost]
        public TaskViewModel UpdateTask(Guid userId, Guid listId, [FromBody][NotNull]TaskViewModel taskViewModel)
        {
            ValidateRequestAttributes(userId, listId);

            var taskDto = _mapper.Map<TaskDto>(taskViewModel);
            var taskDtoUpdated = _taskService.UpdateTask(taskDto);
            var taskViewModelUpdated = _mapper.Map<TaskViewModel>(taskDtoUpdated);
            return taskViewModelUpdated;
        }

        private void ValidateRequestAttributes(Guid userId, Guid listId)
        {
            if (!_userService.IsUserIdExists(userId))
                throw new ArgumentException("User with provided id not found! Could not get task of undefined user.");

            if (_taskListService.IsTaskListExists(listId))
                throw new ArgumentException("TaskList with provided id not found! Could not get tasks of undefined task list.");
        }
    }
}