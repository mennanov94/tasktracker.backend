﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using AutoMapper;
using BusinessLogic.Interfaces.Model;
using BusinessLogic.Interfaces.Service;
using JetBrains.Annotations;
using TaskTracker.Models;

namespace TaskTracker.Controllers
{
    [UsedImplicitly]
    public class TaskListController : ApiController
    {
        private readonly ITaskListService _taskListService;
        private readonly ITaskService _taskService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public TaskListController(
            [NotNull]ITaskListService taskListService,
            [NotNull]ITaskService taskService,
            [NotNull]IUserService userService,
            [NotNull]IMapper mapper)
        {
            _taskListService = taskListService ?? throw new ArgumentNullException(nameof(taskListService));
            _taskService = taskService ?? throw new ArgumentNullException(nameof(taskService));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet]
        public IEnumerable<TaskListViewModel> GetTaskListsByUserId(Guid userId)
        {
            ValidateRequestAttributes(userId);

            return _mapper.Map<IEnumerable<TaskListViewModel>>(_taskListService.GetByUserId(userId));
        }

        [HttpPut]
        public TaskListViewModel CreateTaskList(Guid userId, [FromBody][NotNull]TaskListViewModel taskListViewModel)
        {
            ValidateRequestAttributes(userId);

            var taskListDto = _mapper.Map<TaskListDto>(taskListViewModel);
            var taskListDtoInserted = _taskListService.AddTaskList(taskListDto);
            var taskListViewModelInserted = _mapper.Map<TaskListViewModel>(taskListDtoInserted);
            return taskListViewModelInserted;
        }

        [HttpGet]
        public TaskListViewModel DeleteTaskList(Guid userId, Guid taskListId)
        {
            ValidateRequestAttributes(userId);

            var taskListDto = _taskListService.DeleteTaskList(taskListId);
            var taskListViewModel = _mapper.Map<TaskListViewModel>(taskListDto);
            return taskListViewModel;
        }

        [HttpPost]
        public TaskListViewModel UpdateTaskList(Guid userId, [FromBody][NotNull]TaskListViewModel taskListViewModel)
        {
            ValidateRequestAttributes(userId);

            var taskListDto = _mapper.Map<TaskListDto>(taskListViewModel);
            var taskListDtoUpdated = _taskListService.UpdateTaskList(taskListDto);
            var taskListViewModelUpdated = _mapper.Map<TaskListViewModel>(taskListDtoUpdated);
            return taskListViewModelUpdated;
        }

        private void ValidateRequestAttributes(Guid userId)
        {
            if (!_userService.IsUserIdExists(userId))
                throw new ArgumentException("User with provided id not found! Could not retvieve TaskLists.");
        }
    }
}