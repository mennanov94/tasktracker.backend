﻿using System;
using JetBrains.Annotations;

namespace TaskTracker.Models
{
    [UsedImplicitly]
    public class UserViewModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IconPath { get; set; }
    }
}