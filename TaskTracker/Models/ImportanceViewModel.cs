﻿namespace TaskTracker.Models
{
    public enum ImportanceViewModel
    {
        High = 1,
        Medium = 2,
        Low = 3
    }
}