﻿using System;
using System.Collections.Generic;

namespace TaskTracker.Models
{
    public class TaskListViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public Guid UserId { get; set; }
        public IEnumerable<TaskViewModel> Tasks { get; set; }
    }
}