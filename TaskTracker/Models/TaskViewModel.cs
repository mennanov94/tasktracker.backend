﻿using System;

namespace TaskTracker.Models
{
    public class TaskViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime DueDate { get; set; }
        public ImportanceViewModel Importance { get; set; }
    }
}