﻿using AutoMapper;
using BusinessLogic.Interfaces.Model;
using DataAccess.Interfaces.Model;
using TaskTracker.AutoMapperProfiles;
using TaskTracker.Models;

namespace TaskTracker
{
    public static class AutoMapperConfig
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Importance, ImportanceDto>();
                cfg.CreateMap<ImportanceDto, ImportanceViewModel>();

                cfg.CreateMap<ImportanceDto, Importance>();
                cfg.CreateMap<ImportanceViewModel, ImportanceDto>();

                cfg.AddProfile<UserMappingProfile>();
                cfg.AddProfile<TaskMappingProfile>();
                cfg.AddProfile<TaskListMappingProfile>();
            });

            return config.CreateMapper();
        }
    }
}