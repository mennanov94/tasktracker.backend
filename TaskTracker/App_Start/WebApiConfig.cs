﻿using System.Configuration;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;
using Autofac.Integration.WebApi;

namespace TaskTracker
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            // Web API replace standart dependency resolver with Autofac
            var builder = AutofacConfig.CreateAutofacConfig();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            config.DependencyResolver = new AutofacWebApiDependencyResolver(builder.Build());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
