﻿using System.Configuration;
using Autofac;
using AutoMapper;
using BusinessLogic.Interfaces.Service;
using BusinessLogic.Service;
using DataAccess.EntityFramework.Context;
using DataAccess.EntityFramework.Repository;
using DataAccess.Interfaces.Model;
using DataAccess.Interfaces.Repository;

namespace TaskTracker
{
    public static class AutofacConfig
    {
        public static ContainerBuilder CreateAutofacConfig()
        {

            //var connectionString = ConfigurationManager.ConnectionStrings["TaskTracker"].ConnectionString;
            var connectionString = ConfigurationManager.ConnectionStrings["TaskTrackerWork"].ConnectionString;
            ContainerBuilder builder = new ContainerBuilder();


            builder.RegisterType<TaskTrackerContext>()
                .AsSelf()
                .WithParameter((info, context) => info.ParameterType == typeof(string), (info, context) => connectionString);

            builder.RegisterType<Repository<User>>().As<IRepository<User>>();
            builder.RegisterType<Repository<Task>>().As<IRepository<Task>>();
            builder.RegisterType<Repository<TaskList>>().As<IRepository<TaskList>>();

            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<TaskListService>().As<ITaskListService>();
            builder.RegisterType<TaskService>().As<ITaskService>();

            builder.RegisterInstance(AutoMapperConfig.CreateMapper()).As<IMapper>();

            return builder;
        }
    }
}