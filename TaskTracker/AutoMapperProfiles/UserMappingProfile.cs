﻿using System;
using AutoMapper;
using BusinessLogic.Interfaces.Model;
using DataAccess.Interfaces.Model;
using TaskTracker.Models;

namespace TaskTracker.AutoMapperProfiles
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, UserViewModel>();

            CreateMap<UserViewModel, UserDto>()
                .ForMember(dto => dto.Id, expression =>
                {
                    expression.Condition(model => model.Id == new Guid());
                    expression.MapFrom(model => Guid.NewGuid());
                });
            CreateMap<UserDto, User>();
        }
    }
}