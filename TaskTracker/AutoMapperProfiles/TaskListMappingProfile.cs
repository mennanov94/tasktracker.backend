﻿using System;
using AutoMapper;
using BusinessLogic.Interfaces.Model;
using DataAccess.Interfaces.Model;
using TaskTracker.Models;

namespace TaskTracker.AutoMapperProfiles
{
    public class TaskListMappingProfile : Profile
    {
        public TaskListMappingProfile()
        {
            CreateMap<TaskList, TaskListDto>();
            CreateMap<TaskListDto, TaskListViewModel>();

            CreateMap<TaskListViewModel, TaskListDto>()
                .ForMember(dto => dto.Id, expression =>
                {
                    expression.Condition(model => model.Id == new Guid());
                    expression.MapFrom(model => Guid.NewGuid());
                });
            CreateMap<TaskListDto, TaskList>();
        }
    }
}