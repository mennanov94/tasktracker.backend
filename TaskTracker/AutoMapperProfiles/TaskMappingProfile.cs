﻿using System;
using AutoMapper;
using BusinessLogic.Interfaces.Model;
using DataAccess.Interfaces.Model;
using TaskTracker.Models;

namespace TaskTracker.AutoMapperProfiles
{
    public class TaskMappingProfile : Profile
    {
        public TaskMappingProfile()
        {
            CreateMap<Task, TaskDto>();
            CreateMap<TaskDto, TaskViewModel>();

            CreateMap<TaskViewModel, TaskDto>()
                .ForMember(dto => dto.Id, expression =>
                {
                    expression.Condition(model => model.Id == new Guid());
                    expression.MapFrom(model => Guid.NewGuid());
                })
                .ForMember(dto => dto.DueDate, expression =>
                {
                    expression.Condition(model => model.DueDate == new DateTime());
                    expression.MapFrom(model => DateTime.Now.AddDays(1));
                })
                .ForMember(dto => dto.Importance, expression =>
                {
                    expression.Condition(model => model.Importance < ImportanceViewModel.High);
                    expression.MapFrom(model => ImportanceViewModel.Low);
                });
            CreateMap<TaskDto, Task>();
        }
    }
}