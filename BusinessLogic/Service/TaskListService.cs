﻿using System;
using System.Collections.Generic;
using AutoMapper;
using BusinessLogic.Interfaces.Model;
using BusinessLogic.Interfaces.Service;
using DataAccess.Interfaces.Model;
using DataAccess.Interfaces.Repository;
using JetBrains.Annotations;

namespace BusinessLogic.Service
{
    [UsedImplicitly]
    public class TaskListService : ITaskListService
    {
        private readonly IRepository<TaskList> _repository;
        private readonly IMapper _mapper;

        public TaskListService(
            [NotNull]IRepository<TaskList> repository,
            [NotNull]IMapper mapper)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public TaskListDto AddTaskList(TaskListDto taskList)
        {
            var taskListModel = _mapper.Map<TaskList>(taskList);
            return _mapper.Map<TaskListDto>(_repository.Add(taskListModel));
        }

        public TaskListDto DeleteTaskList(Guid id)
        {
            var taskListModel = _repository.Delete(id);
            var taskListDto = _mapper.Map<TaskListDto>(taskListModel);
            return taskListDto;
        }

        public TaskListDto UpdateTaskList(TaskListDto taskListDto)
        {
            var taskListModel = _mapper.Map<TaskList>(taskListDto);
            var tasklistModelUpdated = _repository.Update(taskListModel);
            var taskListDtoUpdated = _mapper.Map<TaskListDto>(tasklistModelUpdated);
            return taskListDtoUpdated;
        }

        public bool IsTaskListExists(Guid listId)
        {
            return _repository.Get(listId) != null;
        }

        public IEnumerable<TaskListDto> GetByUserId(Guid userId)
        {
            var taskListModelFiltered = _repository.Get(taskList => taskList.UserId == userId);
            var taskListDtoFiltered = _mapper.Map<IEnumerable<TaskListDto>>(taskListModelFiltered);
            return taskListDtoFiltered;
        }

    }
}
