﻿using System;
using System.Collections.Generic;
using AutoMapper;
using BusinessLogic.Interfaces.Model;
using BusinessLogic.Interfaces.Service;
using DataAccess.Interfaces.Model;
using DataAccess.Interfaces.Repository;
using JetBrains.Annotations;

namespace BusinessLogic.Service
{
    [UsedImplicitly]
    public class TaskService : ITaskService
    {
        private readonly IRepository<Task> _repository;
        private readonly IMapper _mapper;

        public TaskService(
            [NotNull]IRepository<Task> repository,
            [NotNull]IMapper mapper)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public TaskDto AddTask(TaskDto task)
        {
            var taskModel = _mapper.Map<Task>(task);
            _repository.Add(taskModel);
            return _mapper.Map<TaskDto>(taskModel);
        }

        public TaskDto UpdateTask(TaskDto task)
        {
            var taskModel = _mapper.Map<Task>(task);
            _repository.Update(taskModel);
            return _mapper.Map<TaskDto>(taskModel);
        }

        public TaskDto GetTask(Guid id)
        {
            var taskModel = _repository.Get(id);
            return _mapper.Map<TaskDto>(taskModel);
        }

        public IEnumerable<TaskDto> GetTask()
        {
            var taskModelList = _repository.Get();
            return _mapper.Map<IEnumerable<TaskDto>>(taskModelList);
        }

        public TaskDto DeleteTask(TaskDto task)
        {
            var taskModel = _mapper.Map<Task>(task);

            return _mapper.Map<TaskDto>(_repository.Delete(taskModel));
        }

        public TaskDto DeleteTask(Guid id)
        {
            var taskModel = _repository.Delete(id);
            return _mapper.Map<TaskDto>(taskModel);
        }

        public IEnumerable<TaskDto> GetTaskByListId(Guid listId)
        {
            var tasksModel = _repository.Get(task => task.TaskListId == listId);
            var tasksDto = _mapper.Map<IEnumerable<TaskDto>>(tasksModel);
            return tasksDto;
        }
    }
}
