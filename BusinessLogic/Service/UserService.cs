﻿using System;
using System.Linq;
using AutoMapper;
using BusinessLogic.Interfaces.Model;
using BusinessLogic.Interfaces.Service;
using DataAccess.Interfaces.Model;
using DataAccess.Interfaces.Repository;
using JetBrains.Annotations;

namespace BusinessLogic.Service
{
    [UsedImplicitly]
    public class UserService : IUserService
    {
        private readonly IRepository<User> _repository;
        private readonly IMapper _mapper;

        public UserService(
            [NotNull]IRepository<User> repository,
            [NotNull]IMapper mapper)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _mapper = mapper ?? throw new ArgumentException(nameof(mapper));
        }

        public UserDto AddUser(UserDto user)
        {
            User userModel = _mapper.Map<User>(user);
            _repository.Add(userModel);
            return _mapper.Map<UserDto>(userModel);
        }

        public UserDto GetUser(Guid id)
        {
            return _mapper.Map<UserDto>(_repository.Get(id));
        }

        public UserDto UpdateUser(UserDto user)
        {
            var userModel = _mapper.Map<User>(user);
            _repository.Update(userModel);
            return _mapper.Map<UserDto>(userModel);
        }

        public bool IsUserIdExists(Guid id)
        {
            var userModel = _repository.Get(id);
            return userModel != null;
        }

        public UserDto GetUserByName(string firstName, string lastName, string middleName = null)
        {
            var userModel = _repository.Get(user => user.FirstName == firstName && user.LastName == lastName).First();
            var userDto = _mapper.Map<UserDto>(userModel);
            return userDto;
        }
    }
}
